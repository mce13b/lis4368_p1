> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Marielle Estrugo

### Project 1 Requirements:

*Parts:*

1. Link
2. Blank Entries
3. Validation
4. Chapter Questions

#### Part 1: Link
[Project 1](http://localhost:9999/lis4368/p1/#)

#### Part 2: Blank Entries

![p1.jpg](https://bytebucket.org/mce13b/lis4368_p1/raw/22d856fa0eb09db87692b09eebd859a32de4f0c5/p1.jpg?token=432993433e7725256165977c90400870ea2983eb)

#### Part 3: Validation

![validation.jpg](https://bytebucket.org/mce13b/lis4368_p1/raw/22d856fa0eb09db87692b09eebd859a32de4f0c5/validation.jpg?token=ac713841c0b9626a81c4a81154985709e0818840)


####Part 4: Chapter Questions
1. A tag that has a body must have

	*  a. an opening tag
	*  b. a body
	*  c. a closing tag
	*  **d. all of the above**

2. Before you can use a custom tag, you must code a taglib directive in the JSP. Within this directive, the URI attribute must specify

	*  a.the location of the tag handler class

3. For each scripting variable, you create a VariableInfo object that provides this data:

	* a. the name and data type of the variable

4. For the data type of a scripting variable, you can specify a

	* a. String object
	* b. any primitive data type
	* c. any wrapper class for a primitive type
	* **d. all of the above**

5. If a tag doesn’t have a body, the doStartTag method should return the

	* c.SKIP_BODY field

6. The number of TLDs an application can have is

	* d.unlimited

7. To be able to access the body of a tag, the tag handler class needs to

	* b.extend the BodyTagSupport class

8. To code a tag handler that uses attributes, you must

	* a.declare a private instance variable for each attribute
	* b.define a set method for each attribute with the standard naming conventions
	* **c.both a and b**


9. To code the equivalent of a Java if statement, you would use the ________________ tag.

	* a. choose
	* b. forEach
	* c. url
	* d. all of the above
	* e. none of the above

10. To code the equivalent of a Java if/else statement, you would use the ________________ tag.

	* b.choose

11. To loop through items in a delimited string, you would use the ________________ tag.

	* c. forTokens

12. To loop through most types of collections, including regular arrays, you would use the ________________ tag.

	* b. forEach

13. You can use custom tags

	* b.with attributes, and with a repeating body

14. To display the body of the tag in the JSP,

	* a.the tag class should return the EVAL_BODY_INCLUDE constant

15. If this code is used for the href attribute of a link,


```
#!java

<c:url value='/cart' >
 <c:param name='productCode' value='8601' />
</c:url>
```

what URL is used for the link?

	* c./cart?productCode=8601


16. If this code is used to parse an attribute named emailAddress that contains a value of
jjones@net@asset.com


```
#!java

<c:forTokens var="part" items="${emailAddress}" delims="@">
	 ${part}<br>
	</c:forTokens>
```

what is displayed in the browser?

	* b.
		jjones
		net
		asset.com